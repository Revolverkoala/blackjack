﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackjackGame.Engine
{
    // A PlayingCard represents a card from a standard, French-style 52-card deck.
    // Just for fun, 53 and 54 are jokers; 55-77 are Tarot trumps

    // https://www.bennadel.com/blog/2240-creating-repeated-sequences-with-the-modulus-mod-operator.htm
    // I've leveraged the above facts in order to represent each card as an integer and derive its rank and suit mathematically
    class PlayingCard
    {
        public int CardID { get; } // Class and suit are derived from this mathematically
        public string Rank { get { return getRankString(CardID); } } // "Ace", "Three", "King"; "Red Joker", "Black Joker"; "The Fool", "The Magician", etc.
        public string Suit { get { return getSuitString(CardID); } } // "[Black|Red] Joker", or "Major Arcana", or the normal suit
        public Lazy<Image> _cardFace = 
        // PRIVATE HELPER FUNCTIONS

        private static string getRankString(int id)
        {
            if (id >= FIRST_TAROT) // it's a Major Arcana
            {
                return TarotStrings[id - FIRST_TAROT]; // 0 is The Fool
            } else if (id >= FIRST_JOKER) // it's a joker
            {
                // Absolutely no reason to do it this way other than consistency
                return JokerStrings[id - FIRST_JOKER];
            } else
            {
                return RankStrings[CardIdToRank(id)];
            }
        }

        private static string getSuitString(int id)
        {
            if (id >= FIRST_TAROT) // It's a tarot
            {
                return MiscStrings["suits_major_arcana"];
            } else if (id >= FIRST_JOKER) // It's a joker
            {
                return JokerStrings[id - FIRST_JOKER]; // return the same string as for rank
            } else
            {
                return SuitStrings[CardIdToSuitId(id)];
            }
        }

        // When building with optimizations (e.g. doing a release build instead of a debug one),
        // the compiler can often get rid of the overhead from calls to tiny functions like this
        // by inlining them: replacing the call with the actual function body, as if you copy-pasted it

        // So GO WILD with tiny functions like this if it makes your code more readable!
        // In Data Structures, you'll learn more about how to detect and fix performance bottlenecks.
        // For now, don't worry about optimizing and focus on making your code ORGANIZED,
        // SELF-DOCUMENTING where possible, and WELL-ENGINEERED. When you do reach a point where
        // it makes sense to go out of your way to optimize, the above will make doing so MUCH easier!
        private static int CardIdToRank(int id)
        {
            return ((id - 1) % 13) + 1;
        }

        private static int CardIdToSuitId(int id)
        {
            return id % 4;
        }

        // PRIVATE DATA MEMBERS

        private const int FIRST_JOKER = 53;
        private const int FIRST_TAROT = 55;

        private static string[] RankStrings =
        {
            "DUMMY", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight"
                , "Nine", "Ten" , "Jack", "Queen", "King"
        };

        private static string[] JokerStrings =
        {
            "Black Joker", "Red Joker"
        };

        private static string[] TarotStrings =
        {
            "0 - The Fool", "I - The Magician", "II - The High Priestess", "III - The Empress"
                , "IV - The Emperor", "V - The Hierophant", "VI - The Lovers", "VII - The Chariot"
                , "VIII - Strength", "IX - The Hermit", "X - Wheel of Fortune", "XI - Justice"
                , "XII - The Hanged Man", "XIII - Death", "XIV - Temperance", "XV - The Devil"
                , "XVI - The Tower", "XVII - The Star", "XVIII - The Moon", "XIX - The Sun"
                , "XX - Judgment", "XXI - The World"
        };

        private static string[] SuitStrings = { "Hearts", "Spades", "Diamonds", "Clubs" };

        private static Dictionary<string, string> MiscStrings = new Dictionary<string, string>
        {
            {"suits_major_arcana", "Major Arcana" }
        };
    }
}
