﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BlackjackGame.Engine
{
    class BlackjackEngine
    {
        const int NUM_DECKS = 6;
        private static Random rng = new Random();
        public int NumCards { get { return 52 * NUM_DECKS; } }
        private Stack<PlayingCard> Deck { get; set; }

        public void NewGame()
        {
            //Shuffle 
        }

        private static Stack<PlayingCard> NewShuffle()
        {
            // Generate a deck with NUM_DECKS of each card
            var deck = new List<PlayingCard>();

            for (int i = 1; i <= 52; i++)
            {
                // Instantiate the card
                var card = new PlayingCard(i);

                for (int j = 0; j < NUM_DECKS; j++)
                {
                    // Add it to the deck NUM_DECKS times
                    deck.Add(card);
                }
            }

            return new Stack<PlayingCard>(deck.OrderBy(a => rng.Next()));
        }
    }
}
