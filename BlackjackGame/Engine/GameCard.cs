﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace BlackjackGame.Engine
{
    // A PlayingCard represents a card from a standard, French-style 52-card deck.
    // Just for fun, 53 and 54 are jokers; 55-77 are Tarot trumps

    // https://www.bennadel.com/blog/2240-creating-repeated-sequences-with-the-modulus-mod-operator.htm
    // I've leveraged the above facts in order to represent each card as an integer and derive its rank and suit mathematically
    class PlayingCard
    {
        private static Random rng = new Random();
        public int CardID { get; } // Class and suit are derived from this mathematically
        public string Rank { get { return getRankString(CardID); } } // "Ace", "Three", "King"; "Red Joker", "Black Joker"; "The Fool", "The Magician", etc.
        public string Suit { get { return getSuitString(CardID); } } // "[Black|Red] Joker", or "Major Arcana", or the normal suit
        public Image Face { get { return CardID >= FIRST_TAROT ? CardImages[0] : CardImages[CardID]; } }
        public int FaceValue { get
            {
                if (CardID > 52)
                    return 0;

                return CardIdToRank(CardID);
            } }
        // TODO: Add virtual FaceValue property, other stuff???

        public PlayingCard(int id)
        {
            if (id > 77)
                throw new ArgumentOutOfRangeException("id", id, "must be between 1 and 77");

            CardID = id;
        }
        // PUBLIC STATIC FUNCTIONS
        public static PlayingCard[] Get52Deck()
        {
            const int NUM_CARDS = 52;

            var deck = new PlayingCard[NUM_CARDS];

            for (int i = 1; i <= NUM_CARDS; i++)
            {
                deck[i] = new PlayingCard(i);
            }

            return deck.OrderBy(a => rng.Next()).ToArray();
        }

        public static PlayingCard[] Get54Deck()
        {
            const int NUM_CARDS = 54;

            var deck = new PlayingCard[NUM_CARDS];

            for (int i = 1; i <= NUM_CARDS; i++)
            {
                deck[i] = new PlayingCard(i);
            }

            return deck.OrderBy(a => rng.Next()).ToArray();
        }
        // PRIVATE HELPER FUNCTIONS

        private static string getRankString(int id)
        {
            if (id >= FIRST_TAROT) // it's a Major Arcana
            {
                return TarotStrings[id - FIRST_TAROT]; // 0 is The Fool
            } else if (id >= FIRST_JOKER) // it's a joker
            {
                // Absolutely no reason to do it this way other than consistency
                return JokerStrings[id - FIRST_JOKER];
            } else
            {
                return RankStrings[CardIdToRank(id)];
            }
        }

        private static string getSuitString(int id)
        {
            if (id >= FIRST_TAROT) // It's a tarot
            {
                return MiscStrings["suits_major_arcana"];
            } else if (id >= FIRST_JOKER) // It's a joker
            {
                return JokerStrings[id - FIRST_JOKER]; // return the same string as for rank
            } else
            {
                return SuitStrings[CardIdToSuitId(id)];
            }
        }

        // When building with optimizations (e.g. doing a release build instead of a debug one),
        // the compiler can often get rid of the overhead from calls to tiny functions like this
        // by inlining them: replacing the call with the actual function body, as if you copy-pasted it

        // So GO WILD with tiny functions like this if it makes your code more readable!
        // In Data Structures, you'll learn more about how to detect and fix performance bottlenecks.
        // For now, don't worry about optimizing and focus on making your code ORGANIZED,
        // SELF-DOCUMENTING where possible, and WELL-ENGINEERED. When you do reach a point where
        // it makes sense to go out of your way to optimize, the above will make doing so MUCH easier!
        private static int CardIdToRank(int id)
        {
            return ((id - 1) % 13) + 1;
        }

        private static int CardIdToSuitId(int id)
        {
            return id % 4;
        }

        // PRIVATE DATA MEMBERS

        private const int FIRST_JOKER = 53;
        private const int FIRST_TAROT = 55;

        private static string[] RankStrings =
        {
            "DUMMY", "Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight"
                , "Nine", "Ten" , "Jack", "Queen", "King"
        };

        private static string[] JokerStrings =
        {
            "Black Joker", "Red Joker"
        };

        private static string[] TarotStrings =
        {
            "0 - The Fool", "I - The Magician", "II - The High Priestess", "III - The Empress"
                , "IV - The Emperor", "V - The Hierophant", "VI - The Lovers", "VII - The Chariot"
                , "VIII - Strength", "IX - The Hermit", "X - Wheel of Fortune", "XI - Justice"
                , "XII - The Hanged Man", "XIII - Death", "XIV - Temperance", "XV - The Devil"
                , "XVI - The Tower", "XVII - The Star", "XVIII - The Moon", "XIX - The Sun"
                , "XX - Judgment", "XXI - The World"
        };

        private static string[] SuitStrings = { "Hearts", "Spades", "Diamonds", "Clubs" };

        private static Dictionary<string, string> MiscStrings = new Dictionary<string, string>
        {
            {"suits_major_arcana", "Major Arcana" }
        };

        private static Image[] CardImages = {
            Properties.Resources.playing_card_back, // 0 = card back
            Properties.Resources.ace_of_spades, // A  S
            Properties.Resources._2_of_diamonds, // 2  D
            Properties.Resources._3_of_clubs, // 3  C
            Properties.Resources._4_of_hearts, // 4  H
            Properties.Resources._5_of_spades, // 5  S
            Properties.Resources._6_of_diamonds, // 6  D
            Properties.Resources._7_of_clubs, // 7  C
            Properties.Resources._8_of_hearts, // 8  H
            Properties.Resources._9_of_spades, // 9  S
            Properties.Resources._10_of_diamonds, // 10 D
            Properties.Resources.jack_of_clubs, // J  C
            Properties.Resources.queen_of_hearts, // Q  H
            Properties.Resources.king_of_spades, // K  S
            Properties.Resources.ace_of_diamonds, // A  D
            Properties.Resources._2_of_clubs, // 2  C
            Properties.Resources._3_of_hearts, // 3  H
            Properties.Resources._4_of_spades, // 4  S
            Properties.Resources._5_of_diamonds, // 5  D
            Properties.Resources._6_of_clubs, // 6  C
            Properties.Resources._7_of_hearts, // 7  H
            Properties.Resources._8_of_spades, // 8  S
            Properties.Resources._9_of_diamonds, // 9  D
            Properties.Resources._10_of_clubs, // 10 C
            Properties.Resources.jack_of_hearts, // J  H
            Properties.Resources.queen_of_spades, // Q  S
            Properties.Resources.king_of_diamonds, // K  D
            Properties.Resources.ace_of_clubs, // A  C
            Properties.Resources._2_of_hearts, // 2  H
            Properties.Resources._3_of_spades, // 3  S
            Properties.Resources._4_of_diamonds, // 4  D
            Properties.Resources._5_of_clubs, // 5  C
            Properties.Resources._6_of_hearts, // 6  H
            Properties.Resources._7_of_spades, // 7  S
            Properties.Resources._8_of_spades, // 8  D
            Properties.Resources._9_of_clubs, // 9  C
            Properties.Resources._10_of_hearts, // 10 H
            Properties.Resources.jack_of_spades, // J  S
            Properties.Resources.queen_of_diamonds, // Q  D
            Properties.Resources.king_of_clubs, // K  C
            Properties.Resources.ace_of_hearts, // A  H
            Properties.Resources._2_of_spades, // 2  S
            Properties.Resources._3_of_diamonds, // 3  D
            Properties.Resources._4_of_clubs, // 4  C
            Properties.Resources._5_of_hearts, // 5  H
            Properties.Resources._6_of_spades, // 6  S
            Properties.Resources._7_of_diamonds, // 7  D
            Properties.Resources._8_of_clubs, // 8  C
            Properties.Resources._9_of_hearts, // 9  H
            Properties.Resources._10_of_spades, // 10 S
            Properties.Resources.jack_of_diamonds, // J  D
            Properties.Resources.queen_of_clubs, // Q  C
            Properties.Resources.king_of_hearts, // K  H
            Properties.Resources.black_joker,
            Properties.Resources.red_joker
        };
    }
}
